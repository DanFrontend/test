//- Import
import axios from 'axios'

//- Instance
const instance = axios.create({
  baseURL: '/',
  headers: {
    'Content-type': 'application/json',
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*'
  },
})

export const useApi = instance
